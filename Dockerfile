FROM tensorflow/tensorflow:1.9.0-gpu-py3

## Use below image for CPU only environment
#FROM tensorflow/tensorflow:1.9.0-py3

VOLUME /notebooks

## Object Detection API dependencies
RUN apt-get update && apt-get install -y \
    protobuf-compiler \
    python3-pil \
    python3-lxml \
    python3-tk

RUN pip3 install \
    Cython \
    contextlib2 \
    jupyter \
    matplotlib \
    pycocotools

### Assuming models repository is cloned under /notebooks
ENV PYTHONPATH=$PYTHONPATH:/notebooks/models/research/:/notebooks/models/research/slim

## Install TensorBoard
RUN pip3 install tensorboard

## Install Jupyter Lab
RUN pip3 install jupyterlab

## Intall Jupyter Lab Extension
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash -
RUN apt-get install -y nodejs
RUN jupyter labextension install jupyterlab_vim

## Run Jupyter Lab instead of Jupyter Notebook
CMD jupyter lab --allow-root
