# TensorFlow Docker Workspace
TensorFlowのDockerImageを使って作業するワークスペースです。
このレポジトリを使ってTensorFlowでの作業を簡単に始められます。

このワークスペースでは、TensorFlowのDocker Imageにもともと入っているJupyter Notebookではなく、
Jupyter Labを使用することができます。

## 準備

### Setup NVIDIA display driver
http://gihyo.jp/admin/serial/01/ubuntu-recipe/0454

PPAからインストールする

```
sudo add-apt-repository ppa:graphics-drivers/ppa
sudo apt update
sudo apt install nvidia-384 #最新のもので良い
```

### Setup nvidia-docker
https://github.com/NVIDIA/nvidia-docker/wiki/Installation

Install Docker
```
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

sudo apt-get update
sudo apt-get install docker-ce
```

Install nvidia-docker

このブログを参考に入れる
http://blog.amedama.jp/entry/2017/04/03/235901

nvidia-modprobeを先に入れておかないと、nvidia-dockerサービスの起動に失敗する
```
sudo apt install nvidia-docker
```

```
wget https://github.com/NVIDIA/nvidia-docker/releases/download/v1.0.1/nvidia-docker_1.0.1-1_amd64.deb
sudo dpkg -i nvidia-docker_1.0.1-1_amd64.deb
```

### Build docker image
```
bash docker-build.sh
```

このDockerfileでは、Jupyter LabにVim拡張をインストールします。
Vim拡張が必要ない場合には、Dockerfile内の

```
RUN jupyter labextension install jupyterlab_vim
```

の行を削除してから上記のコマンドを実行してください。

## Dockerイメージの起動
```
./docker-run.sh
```

ブラウザで
```
http://x.x.x.x:8888/lab
```
にアクセスするとJupyter Labにアクセスできます。
※コンソールに出力されているtokenを入れる必要があります

## その他

Docker containerに入るには
```
./docker-exec-bash.sh
```
を使用してください。

TensorBoardを使用する場合にはDocker containerに入った後に
```
tensorboard --logdir=LOG_DIR
```
と入力し、ブラウザで
```
http://x.x.x.x:6006/
```
にアクセスするとTensorBoardにアクセスできます。

